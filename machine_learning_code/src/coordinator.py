
from preprocessing import *
from server.data import dblayer
from colorama import init, Fore, Back, Style
from featureselection import *

import numpy as np
import sys
import math
import time

PREDICTION_CHUNK_SIZE = 100000

def get_time_stamp():
    return int(time.time())

def perform_fs(array_name, features, labeled_feature, labels,
        preprocessing_options, feature_selection_method, classifier,
        feature_selection_options=None, classification_options=None):

    print "Stage 1: Start"

    # Loading data from db
    array_attrs = [(aa[0], aa[1])
            for aa in dblayer.get_array_attributes(array_name)
            if aa[0] in [dblayer.serialize_string(f) for f in features]]
    labeled_attr = [(aa[0], aa[1])
            for aa in dblayer.get_array_attributes(array_name)
            if aa[0] == dblayer.serialize_string(labeled_feature)]

    try:
        data = np.array(dblayer.get_training_data(
            array_name, [aa[0] for aa in array_attrs] + [labeled_feature],
            labels))
    except Exception as err:
        print (Fore.RED)
        print "Error fetching data."
        print(Fore.RESET)
        return {'error': True,
                'message': "Error recieving data! Training data may be exceeding the memory limit, please contact server admin or choose a smaller dataset."}
        
    array_data = data[:,:-1]
    labeled_data = np.array([d[0] for d in data[:,-1:]])

    print "Stage 2: Data recieved"

    # Perform preprocessing
    pp = Preprocessing(preprocessing_options)

    array_data, labeled_data, array_attrs, labeled_data_info, removed_attrs = \
            pp.perform_training_pp(array_data, labeled_data, array_attrs, labeled_attr)

    if labeled_data_info == False:
        return {'error': True, 'message': labeled_data}
    
    print "Stage 3: Preprocessing finished"

    # Perform feature selection and create classification model
    if feature_selection_method == 'RFE':
        fs = RecursiveFeatureElimination(classifier, pp,
            feature_selection_options)
        score = fs.perform_train_test(array_data, labeled_data)

    elif feature_selection_method == 'RFECV':
        fs = RecursiveFeatureEliminationCrossValidation(classifier, pp)
        score = fs.perform_train_test(array_data, labeled_data)

    elif feature_selection_method == 'EFS':
        fs = ExhaustiveFeatureSelection(classifier, pp)
        score = fs.exhaustive_search(array_data, labeled_data)


    elif feature_selection_method == 'RFS':

        # CALCULATING RANDOM MEASUREMENT ERROR MATRIX,
        #THIS NEEDS TO CHANGE FOR PERFORMING REAL RFS
        data_errors = np.random.normal(0.0, 0.1, array_data.shape)
        label_errors = np.random.normal(0.0, 0.1, labeled_data.shape)
        ###########################################################################################

        fs = RobustFeatureSelection(classifier, pp)
        score = fs.robust_selection(array_data, data_errors, labeled_data,
                label_errors)

    masked_features = fs.get_masked_features()
    feature_ranking = fs.get_feature_ranking()

    print "Stage 4: Classification model created"

    fs_result = {
        'score': score,
        'array_attrs': array_attrs,
        'selected_features': fs.get_masked_features(),
        'labels': labeled_data_info['labels'].tolist()
    }

    # Store features and model
    store_array_name = array_name + '_' + str(get_time_stamp())
    stored = dblayer.create_fs_array(store_array_name, array_attrs,
            masked_features, feature_ranking, feature_selection_method,
            classifier, labeled_feature, labels, score, removed_attrs)

    if stored == False:
        return {'error': True, 'message': 'Failed to create array.'}

    fs.save_model(dblayer.serialize_string(store_array_name))

    print "Stage 5: Everything done!"

    return {'error': False, 'message': fs_result}


def perform_prediction(array_name, classification_model, attr_name):

    global STRING_FEATURES_SEPARATOR

    print "Stage 1: Start"

    fs_dict = dblayer.get_fs_array(classification_model)
    temp_array_name = array_name + '_' + dblayer.random_string(12)
    result = dblayer.create_scidb_file(temp_array_name)

    if result == False:
        return False

    # Parse information to be able to load data
    features = [fd[0] for fd in fs_dict['attributes'] if fd[1] == 1]
    features_splitted = [f.split(STRING_FEATURES_SEPARATOR, 1)[0]
            for f in features]
    features_uniq = []
    [features_uniq.append(item) for item in features_splitted
            if item not in features_uniq]

    # Load classification model
    fs = load_model(dblayer.serialize_string(classification_model))

    # Get data
    array_high_dim = dblayer.get_array_dimensions(array_name)[0][6]
    nr_of_samples = float(array_high_dim)
    chunk_size = float(PREDICTION_CHUNK_SIZE)
    chunks = int(math.ceil(nr_of_samples/chunk_size))

    label_count = {}
    for l in fs_dict['labels']:
        label_count[l] = 0

    for i in range(0, chunks):
        print "Chunk nr: " + str(i+1) + "/" + str(chunks)

        low_coord = int(i * chunk_size)

        if i == (chunks-1):
            high_coord = array_high_dim
        else:
            high_coord = int((i * chunk_size) + chunk_size - 1)

        array_attrs = [(aa[0], aa[1])
                for aa in dblayer.get_array_attributes(array_name)
                if aa[0] in [dblayer.serialize_string(f) for f in features_uniq]]

        if len(array_attrs) != len(features_uniq):
            return {
                'error': True,
                'message': 'ERROR! The chosen features for this classification models does not exist in the dataset.'
            }

        array_data, labeled_data, label_dtype = \
                dblayer.get_prediction_data(array_name,
                        [aa[0] for aa in array_attrs] + [fs_dict['label']],
                        fs_dict['labels'], low_coord, high_coord)

        if len(array_data) < 1:
            return {
                'error': True,
                'message': 'ERROR! No prediction data were to be found.'
            }

        array_data = np.array([row[:-1] for row in array_data])
        labeled_data = np.array([la[0] for la in labeled_data])

        array_data, array_attrs = fs.Preprocessing.perform_prediction_pp(
            array_data,
            [(dblayer.deserialize_string(aa[0]), aa[1]) for aa in array_attrs],
            features, fs_dict['attributes'])

        # Check if estimator needs all features or not
        if fs_dict['method'] == 'RFE' or fs_dict['method'] == 'RFECV':

            array_data_new = np.zeros((array_data.shape[0], 1))
            for a in fs_dict['attributes']:
                if a[1] == 0:
                    array_data_new = np.column_stack((array_data_new,
                        np.zeros((array_data.shape[0], 1))))
                elif a[1] == 1:
                    index = find_element_in_list(a[0],
                            [aa[0] for aa in array_attrs])
                    array_data_new = np.column_stack((array_data_new,
                        array_data[:,index]))

            array_data = array_data_new[:,1:]

        # Decode labels
        predicted_labels = fs.Preprocessing.label_decoder(fs.predict(array_data))

        for pl in predicted_labels:
            label_count[pl] += 1

        pl_index = 0
        for ld_index in range(len(labeled_data)):
            if labeled_data[ld_index] not in fs_dict['labels']:
                labeled_data[ld_index] = predicted_labels[pl_index]
                pl_index += 1

        result = dblayer.write_scidb_file(temp_array_name, [[pl] for pl in labeled_data])

        if result == False:
            return {
                'error': True,
                'message': 'ERROR! Failed to create prediction labels.'
            }

        print " "

    print "New attribute is being added."

    result = dblayer.add_new_attribute(array_name, temp_array_name, attr_name, label_dtype)

    dblayer.remove_temp_file(temp_array_name)

    if result:
        print "SUCCESS! Attribute has been added."
        dblayer.remove_array(array_name)
        dblayer.rename_array(temp_array_name, array_name)
        return {
            'error': False,
            'label_count': label_count,
            'feature_name': dblayer.deserialize_string(attr_name),
            'dataset_name': array_name
        }
    else:
        print "ERROR! Failed to add new feature."
        return False






