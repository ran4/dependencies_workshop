
from sklearn.preprocessing import Imputer, OneHotEncoder, MinMaxScaler, StandardScaler, LabelEncoder

import numpy as np


MAXIMUM_CATEGORIAL_FEATURE_CONSTRUCTION = 20
MAXIMUM_NUMBER_OF_LABELS = 3
STRING_FEATURES_SEPARATOR = '=:='

def find_element_in_list(element, list_element):
    try:
        index_element = list_element.index(element)
        return index_element
    except ValueError:
        return -1

class Preprocessing(object):

    def __init__(self, preprocessing_options):

        self.preprocessing_options = preprocessing_options
        self.encoder = OneHotEncoder(sparse=False)
        self.standard_scaler = StandardScaler()
        self.min_max_scaler = MinMaxScaler()
        self.label_encoder = LabelEncoder()
        self.imputer_stats = [0.0] * 1000

    # Imputation of missing values, strategies are ['remove', mean', 'median', 'most_frequent']
    def missing_value_imputation(self, array_data, array_attrs, prediction, strategy='mean'):
        
        def test_none_type(value):
            if type(value) == type(None):
                return True
            else:
                return False

        # Create array with sum of none/null values
        vfunc = np.vectorize(test_none_type)
        none_type_matrix = vfunc(array_data)
        none_sum = np.sum(none_type_matrix == True, axis=0)
        tot_row_count = array_data.shape[0]
        
        index_addon = 0

        for ns_index in range(len(none_sum)):

            if none_sum[ns_index] > 0:

                new_index = ns_index + index_addon

                if strategy == 'remove': # If option is to remove none samples
                    array_attrs = array_attrs[:new_index] + array_attrs[new_index+1:]
                    array_data = np.c_[array_data[:,:new_index], array_data[:,new_index+1:]]
                    index_addon -= 1
                else:
                    if none_sum[ns_index] == tot_row_count: # If option is to calculate imputation but no value exist
                        if prediction == True: # If prediction time you cannot remove the step but calculate values from training set
                            averaged_column = [[self.imputer_stats[new_index]] for ad in array_data[:,new_index]]
                            array_data = np.c_[array_data[:,:new_index], averaged_column, array_data[:,new_index+1:]]
                        else:
                            array_attrs = array_attrs[:new_index] + array_attrs[new_index+1:]
                            array_data = np.c_[array_data[:,:new_index], array_data[:,new_index+1:]]
                            index_addon -= 1
                    else: # If option is to calculate imputation and value exist
                        imputer = Imputer(missing_values='NaN', strategy=strategy, axis=0)
                        averaged_column = imputer.fit_transform([[ad] for ad in array_data[:,new_index]])
                        self.imputer_stats[new_index] = imputer.statistics_[0]
                        array_data = np.c_[array_data[:,:new_index], averaged_column, array_data[:,new_index+1:]]


        return array_data, array_attrs




    # Encoding categorial features
    def categorial_feature_transformation(self, array_data, array_attrs):

        global MAXIMUM_CATEGORIAL_FEATURE_CONSTRUCTION, STRING_FEATURES_SEPARATOR

        restart = True

        while restart:

            for aa_index in range(len(array_attrs)):

                if array_attrs[aa_index][1] == 'string':

                    # Translate categorial values to numerical
                    original_string_data = [a[aa_index] for a in array_data]
                    indexed_string_data, indexed_numeric = np.unique(original_string_data, return_inverse=True)
                    indexed_numeric = [[ic] for ic in indexed_numeric]

                    # Add new features
                    if np.max(indexed_numeric) + 1 > MAXIMUM_CATEGORIAL_FEATURE_CONSTRUCTION:
                        array_attrs = array_attrs[:aa_index] + array_attrs[aa_index+1:]
                        array_data = np.c_[array_data[:,:aa_index], array_data[:,aa_index+1:]]
                    else:
                        transformed_feature = self.encoder.fit_transform(indexed_numeric)
                        array_attrs = array_attrs[:aa_index] + [(array_attrs[aa_index][0] + STRING_FEATURES_SEPARATOR + str(isd), 'float') for isd in indexed_string_data] + array_attrs[aa_index+1:]
                        array_data = np.c_[array_data[:,:aa_index], transformed_feature, array_data[:,aa_index+1:]]

                    break

                elif aa_index == len(array_attrs)-1:
                    restart = False
                    break

        return array_data, array_attrs

    # Min-max scaling
    def min_max_scale(self, array_data):
        
        return self.min_max_scaler.fit_transform(array_data)


    # Standardization with mean removal and variance scaling
    def standard_scale(self, array_data):
        
        return self.standard_scaler.fit_transform(array_data)


    # Label transformation
    def label_transformation(self, labeled_data):
        
        labeled_data = self.label_encoder.fit_transform(labeled_data)
        labels = self.label_encoder.classes_

        return labeled_data, labels

    def label_decoder(self, encoded_labels):

        return self.label_encoder.inverse_transform(encoded_labels)



    # Perform preprocessing pipeline
    def perform_training_pp(self, array_data, labeled_data, array_attrs, labeled_attr):

        global MAXIMUM_NUMBER_OF_LABELS

        removed_attrs = []

        array_data, array_attrs = self.categorial_feature_transformation(array_data, array_attrs)
        old_array_attrs = array_attrs

        array_data, array_attrs = self.missing_value_imputation(array_data, array_attrs, False, self.preprocessing_options['imputation_strategy'])

        if len(array_attrs) == 0:
            return array_data, 'The dataset for the given features contains no data', array_attrs, False, None
        elif len(array_attrs) < len(old_array_attrs):

            removed_attrs = [oaa[0] for oaa in old_array_attrs if oaa[0] not in [aa[0] for aa in array_attrs]]

        array_data = array_data.astype(float)

        # Min-max scaling
        if self.preprocessing_options['standard']:
            array_data = self.standard_scale(array_data)
        else: # Else min-max scaling
            array_data = self.min_max_scale(array_data)
        
        labeled_data_info = {}
        labeled_data, labels = self.label_transformation(labeled_data)

        for lc in np.bincount(labeled_data):
            if lc < MAXIMUM_NUMBER_OF_LABELS:
                return array_data, 'Class population needs to be larger than ' + str(MAXIMUM_NUMBER_OF_LABELS) + '.', array_attrs, False, None

        if len(labels) < 2:
            return array_data, 'The number of classes must be at least two.', array_attrs, False, None

        labeled_data_info['name'] = labeled_attr[0][0]
        labeled_data_info['dtype'] = labeled_attr[0][1]
        labeled_data_info['labels'] = labels
        
        return array_data, labeled_data, array_attrs, labeled_data_info, removed_attrs


    def perform_prediction_pp(self, array_data, array_attrs, training_attrs, all_training_attrs):

        array_data, array_attrs = self.categorial_feature_transformation(array_data, array_attrs)

        array_attrs_names = []
        array_attrs_index = []
        standardization_list = []
        for aa_index in range(len(array_attrs)):
            if array_attrs[aa_index][0] in training_attrs:
                array_attrs_names.append(array_attrs[aa_index][0])
                array_attrs_index.append(aa_index)

        # Transform to new data array
        array_data_new = []
        for ta_index in range(len(training_attrs)):
            index = find_element_in_list(training_attrs[ta_index], array_attrs_names)
            if index > -1:
                if len(array_data_new) < 1:
                    array_data_new = [[ad] for ad in array_data[:,index]]
                else:
                    array_data_new = np.column_stack((array_data_new, array_data[:,index]))
            else:
                if len(array_data_new) < 1:
                    array_data_new = np.zeros((array_data.shape[0], 1))
                else:
                    array_data_new = np.column_stack((array_data_new, np.zeros((array_data.shape[0], 1))))

        # Transform new attributes array and standardization values
        array_attrs_new = []
        for ta in training_attrs:
            index = find_element_in_list(ta, [aa[0] for aa in array_attrs])
            if index > -1:
                array_attrs_new.append(array_attrs[index])
            else:
                array_attrs_new.append((ta, 'string'))

        array_data_new, array_attrs_new = self.missing_value_imputation(np.array(array_data_new), array_attrs_new, True, self.preprocessing_options['imputation_strategy'])
        array_data_new = array_data_new.astype(float)

        if self.preprocessing_options['standard']: # Standard scaler
            
            mean_new = []
            std_new = []
            mean = self.standard_scaler.mean_
            std = self.standard_scaler.std_

            for ta in training_attrs:
                index = find_element_in_list(ta, [aa[0] for aa in all_training_attrs])
                if index > -1:
                    mean_new.append(mean[index])
                    std_new.append(std[index])
                else:
                    mean_new.append(0.0)
                    std_new.append(1.0)

            array_data_new = (array_data_new - mean_new) /  std_new

        else: # Else min-max scaling
            
            min_new = []
            scale_new = []
            min_ = self.min_max_scaler.min_
            scale = self.min_max_scaler.scale_

            for ta in training_attrs:
                index = find_element_in_list(ta, [aa[0] for aa in all_training_attrs])
                if index > -1:
                    min_new.append(min_[index])
                    scale_new.append(scale[index])
                else:
                    min_new.append(0.0)
                    scale_new.append(1.0)

            array_data_new = (array_data_new - min_new) *  scale_new

        return array_data_new, array_attrs_new







